> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**

# LIS4368

## Chris Curry

### Assignment #3 Requirements:

[a3.mwb file](A3.mwb)
[a3.sql file](a3.sql)

#### Assignment Screenshots:

![erd screenshots](img/a3erd.png)
