> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**

# LIS4368

## Chris Curry

### Assignment #4 Requirements:

1. Course Title, name, assignment requirements.
2. Screenshots
3. Ch Questions (11,12)


#### Assignment Screenshots:

![erd screenshots](img/a4_screenshot.png)

* Screenshot of passed validation;

![erd screenshots](img/a4_pass.png)