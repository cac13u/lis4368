> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**

# LIS4368

## Chris Curry

### Project #2 Requirements:

1. MVC Framework
2. Client-side server validation
3. JSTL for XSS attacks
4. Crud functionality

README.md file should include the following items:

1)Screenshot of Valid User Form Entry (customerform.jsp)
2)Screenshot of Passed Validation (thanks.jsp)
3)Screenshot of Display Data (modify.jsp)
4)Screenshot of Modify Form (customer.jsp)
5)Screenshot of Modified Data (modify.jsp)
6)Screenshot of Delete Warning (modify.jsp)
7)Screenshot of Database changes

#### Assignment Screenshots:

![erd screenshots](img/p2_1.png)

![erd screenshots](img/p2_2.png)

![erd screenshots](img/p2_3.png)

![erd screenshots](img/p2_4.png)

![erd screenshots](img/p2_5.png)

![erd screenshots](img/p2_6.png)

![erd screenshots](img/p2_7.png)


