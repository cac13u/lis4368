LIS 4368 - Advanced Web Applications Development

##### Assignments:

1)[A1 README.md](a1/README.md)

  * Basic java program
  * Set up - Distributed Version Control with Git and Bitbucket
  * Set up - Java/JSP/Serviet Development Installation
  * Set up - tomcat
  * Learn git commands
  * Create two more repos, and offer links for them

2)[A2 README.md](a2/README.md)

  * Installing and using mysql
  * Creating a new user on mysql
  * Grant permissions to users
  * Creating tables, and adding information in mysql
  * Use @WebServlet (annotation) declares servlet configuration
  * Create a hello page, query page, and a query results

3)[A3 README.md](a3/README.md)

  * Create ERD based upon business rules
  * Provide screenshot of completed ERD
  * Provide DB resource links

4)[A4 README.md](a4/README.md)

  * Data Validation
  * Use of Java/CSS/HTML5
  * Debug
  * Upload files to repository

5)[A5 README.md](a5/README.md)

  *Basic Server Side Validation

  *Prevent SQL injections

  *insert functionality to A4

6)[Project 1: README.md](project1/README.md)

  * Create index page, with carousel
  * Create working links for other folders
  * Create P1 index page
  * Validate user input to each specific type
  * Give error messages when a user inputs wrong values
  * Show user when input has been properly inputed

7)[Project 2: README.md](p2/README.md)

  * MVC Framework

  * Client-side server validation

  * Prepared Statements for SQL injections

  * JSTL to prevent XSS

  * CRUD functionality

Directories to know:
/Applications/AMPPS/mysql/bin
/Applications/tomcat/webapps

Things to know:
main sql username/password: root/mysql
