> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**

# LIS4368

## Chris Curry

### Assignment #2 Requirements:

[http://localhost:9999/hello](http://localhost:9999/hello)

[http://localhost:9999/hello/HelloHome.html](http://localhost:9999/hello/HelloHome.html)

[http://localhost:9999/hello/sayhello](http://localhost:9999/hello/sayhello)

[http://localhost:9999/hello/querybook.html](http://localhost:9999/hello/querybook.html)

[http://localhost:9999/hello/sayhi](http://localhost:9999/hello/sayhi) 

#### Assignment Screenshots:

![querybook.html screenshots](img/querybook.png)

