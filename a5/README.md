> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**

# LIS4368

## Chris Curry

### Assignment #5 Requirements:

1. Course Title, name, assignment requirements.
2. Screenshots
3. Ch Questions (13,15)


#### Assignment Screenshots:

![erd screenshots](img/A5_1.png)

* Screenshot of passed validation;

![erd screenshots](img/A5_2.png)

![erd screenshots](img/A5_3.png)